// @flow
export { default } from './components/Modal';
export {
  Header as ModalHeader,
  Footer as ModalFooter,
  Title as ModalTitle,
} from './styled/Content';
