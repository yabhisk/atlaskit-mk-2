import { UploadParams } from '@atlaskit/media-core';

export const defaultUploadParams: UploadParams = {
  collection: '',
  fetchMetadata: true,
  autoFinalize: true,
};
