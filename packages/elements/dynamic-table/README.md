# DynamicTable

Table component with pagination and sorting functionality.

![DynamicTable](http://i.imgur.com/H3Z93jk.png)

## Try it out

Interact with a [live demo of the @atlaskit/dynamic-table component](https://atlaskit.atlassian.com).

## Installation

```sh
yarn add @atlaskit/dynamic-table
```

Detailed docs and example usage can be found [here](https://atlaskit.atlassian.com).
