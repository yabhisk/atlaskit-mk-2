# RadioGroup

Provides a standard way to select a single option from a list.

## Installation

npm install @atlaskit/field-radio-group

## Usage

See [here](https://ak-mk-2-prod.netlify.com/mk-2/packages/elements/field-radio-group)
