# Contributing

Although Atlaskit is responsible for the main development of their components, contributions from outside of our team account for a major part of our development. There are two types of contributions that we are capable of accepting:

1. Open source
2. Inter-team

We haven't had any major open source contributions yet, but we'll describe the process we want to follow here as it underpins the inter-team model.

## Open source contribution model

We want to keep this model very simple. At it's core, we make the assumptions that developers looking to contribute will look at the `README.md` and thus, be directed to the `CONTRIBUTING.md`. Even so, both of those files are widely known as convention in an open source project and developers will be able to direct themselves there.

The process is very simple and consists of:

1. Raise an issue for discussion.
2. Submit a PR resolving the raised issue.
3. Discuss.
4. Merge when ready.

This is super simple and the PR guidelines can be derived from the rest of our conventions and documentation. It's also compatible with the inter-team model, because, at the very worst, internal teams would still be able to follow this process.

The only real downsides here are that:

1. It can take longer to yield a merge due to async discussions.
2. Discovery of contribution need is mostly up to the contributor, unless we reach out to them.

The inter-team model expands on the open source model to make this process more efficient because we have the privilege of working with internal teams in such a way.

## Inter-team contribution model

Like the open source model, the inter-team contribution model is kept as light as possible and is designed for compatibility with it so that, at worst, we can fallback to the open source model as teams in other timezones may not be able to meet as easily.

The steps for the inter-team model are as follows.

### Introduction

The contribution may be initially sparked by several methods:

- Water-cooler discussion with a colleague.
- Issue raised by another team.
- Atlaskit team member noticing similar implementations in products and raising it.
- Any sync meetings between teams.

### Initial discussion

Regardless of how the discussion is triggered, we want to be clear on a few things:

1. Any initial action items.
2. Decide who will be the contributor from the external team.
3. Decide who will be the shepherd from within the Atlaskit team.

This process may come naturally and not require an initial meeting. For example, a contribution may be very simple, a PR may have already been made and it may be good to go out of the gate. Others may require a bit more formality to keep the quality bar high.

A more complete process may look like: introduction > discuss approach and plan > spec / prototype review > raise PR > discuss and merge > high five.

_We don't want this process to to impede the contribution, waste anyone's time or to appear like the waterfall process. You should decide up front if the complexity if the contribution warrants such process and always be mindful to keep it as lean as possible._

### Regular catch-ups

Since these two components can affect one another, everyone involved should keep in constant communication. Therefore it's a good idea if a regular catch-up is scheduled between interested parties to keep the feedback loop tight. These catchups may be simply stand-ups, or might be more involved and contain demos of work progress. The complexity of these will be unique for each contribution.

Some examples of this are:

- Daily stand-ups
- Weekly catch-ups
- Demos every other day

The people involved in these catch-ups will vary, as well. For example, it may simply be the contributor and shepherd. It may have a designer present. Other team members may be involved depending on the complexity.

_The key here is that everyone shares knowledge and can hold each other accountable as everyone may have several things on their plate at any given point in time._

### Shepherd involvement

Every contribution will have a shepherd, but to the extent at which this shepherd will be involved will vary. For example, a shepherd may not be a developer at all, or might decide that part-time development is a good idea. A shepherd may even take over development at some point. Whatever the case, the shepherd must at the very least be dedicated to:

- Reviewing relevant PRs.
- Meeting with contributors, or involved parties.
- Planning any follow ups after the final contribution and handover has been made.

### Tracking

The shepherd should raise an issue internally to track the overall process and give it a label of "contribution".
