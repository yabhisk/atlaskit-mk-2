import { expect } from 'chai';
import PluginSlot from '../../../../src/editor/ui/PluginSlot';

describe('@atlaskit/editor-core/editor/plugins/PluginSlot', () => {
  it('should have PluginSlot component defined', () => {
    expect(PluginSlot).to.not.equal(undefined);
  });
});
