# TagGroup

This component displays an arbitrary number of
[`@atlaskit/tag`](https://www.npmjs.com/package/@atlaskit/tag)s in a grouped
manner. The group manages spacing and animation direction and allows for some
alignment options.

![Example tag group](https://i.imgur.com/A10xBnV.gif)
