import styled from 'styled-components';
import Button from '@atlaskit/button';

export const Wrapper = styled.div`
  display: flex;
  justify-content: flex-end;

  height: 80px;
  padding: 26px 15px 23px 18px;
`;

export const InsertButton = styled(Button)`
  margin-right: 5px;
` as any;

export const CancelButton = styled(Button)`` as any;
