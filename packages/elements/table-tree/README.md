# Table Tree

Use this component to display an expandable table with a tree-like hierarchy.

![Example table tree](https://i.imgur.com/jcJeeuc.gif)
