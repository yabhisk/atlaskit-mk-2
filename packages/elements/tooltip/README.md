# Tooltip

Use this component to display extra information about an element by displaying a floating description.

![Example tooltip](https://i.imgur.com/vlvguzg.gif)
