// @flow
import React from 'react';
import Tag from '../src';

export default () => {
  return (
    <Tag
      href="https://www.atlassian.com/search?query=Carrot%20cake"
      text="Carrot cake"
    />
  );
};
