# @atlaskit/conversation

## 5.0.0
- [major] Added some tests for reducer/store and renamed actions [ba629ef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba629ef)

## 4.9.1

## 4.9.0
- [minor] Adding optimistic updates [45922f3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/45922f3)

## 4.8.2

## 4.8.1
- [patch] Fixes the avatar next to the editor and hides reply functionality if user isn't set [0123c1d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0123c1d)

## 4.8.0



- [minor] Add optional onUserClick handler [40f2e90](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/40f2e90)

## 4.7.1

## 4.7.0
- [minor] Add emoji/mention support [846baed](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/846baed)

## 4.6.0
- [minor] Fix delete comment to accept 204 [83b5f70](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/83b5f70)

## 4.5.0
- [minor] Add comment delete functionality [e26446a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e26446a)

## 4.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 4.3.0
- [minor] feature/ED-3471 Add comment edit functionality [c474f67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c474f67)

## 4.2.1

## 4.2.0

## 4.1.0

## 4.0.0
- [major] New API [41633b9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41633b9)

## 3.0.11

## 3.0.10
- [patch] Fix dependencies in CI [670e930](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/670e930)

## 3.0.9


- [patch] dummy changeset to initiate release [ba17f5b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba17f5b)

## 3.0.8

## 3.0.7

## 3.0.6

## 3.0.5

## 3.0.4

## 3.0.3

## 3.0.2

## 3.0.1

## 3.0.0
- [major] Changing the API to match the service [b308326](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b308326)

## 2.0.7

## 2.0.6

## 2.0.5

## 2.0.4

## 2.0.3

## 2.0.2

## 2.0.1

## 2.0.0

- [major] Conversation Component [bc1a3a4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bc1a3a4)
