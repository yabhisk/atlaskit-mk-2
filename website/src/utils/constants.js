/* @flow */

export const OLD_WEBSITE_URL = 'https://atlaskit.atlassian.com/';
export const NEW_WEBSITE_PREFIX = 'mk-2';
