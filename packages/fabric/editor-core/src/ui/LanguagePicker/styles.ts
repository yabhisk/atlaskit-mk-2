import styled from 'styled-components';
import ToolbarButtonDefault from '../ToolbarButton';
import { ComponentClass } from 'react';

// tslint:disable-next-line:variable-name
export const TrashToolbarButton: ComponentClass<any> = styled(
  ToolbarButtonDefault,
)`
  width: 24px;
`;
