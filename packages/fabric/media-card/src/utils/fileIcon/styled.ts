/* tslint:disable:variable-name */
import styled from 'styled-components';
import { size } from '../../styles';

export const FileTypeIcon = styled.div`
  float: left;
  margin-right: 6px;
  position: relative;
  top: 1px;
  img {
    ${size('12px !important')};
  }
`;
