// @flow
import React from 'react';
import { md, Props } from '@atlaskit/docs';

export default md`
  Use lozenges to highlight an item's status for quick recognition. Use
  subtle lozenges by default and in instances where they may dominate the
  screen, such as in long tables.

  ${<Props props={require('!!extract-react-types-loader!../src/')} />}
`;
