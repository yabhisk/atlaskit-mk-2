# Page

Provides Grid layouts for content & components

## Installation

npm install @atlaskit/page

## Usage

See [here](https://ak-mk-2-prod.netlify.com/mk-2/packages/elements/page)
