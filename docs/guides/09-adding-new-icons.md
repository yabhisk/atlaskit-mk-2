# Adding New Icons

!!IMPORTANT

The icons package has a custom build process, as it generates its both stripped
svgs and glyphs that are committed to the repo, so that they can be accessed as
paths when published.

You will manually need to run `yarn build:icon` from the root repository, or
`yarn build` from inside the icon folder whenever you make changes to icon.

## Generating an svg

## Building stripped svg and glyphs

## Verifying and committing your new icon
